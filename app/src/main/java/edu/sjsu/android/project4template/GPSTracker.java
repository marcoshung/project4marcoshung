package edu.sjsu.android.project4template;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

/**
 * Class for get current location
 * Check and set location service by LocationManager + Dialog + Intent
 * And get location by FusedLocationProviderClient of Google Location Services
 */
class GPSTracker {
    private final Context context;
    public GPSTracker(Context context) {
        this.context = context;
    }

    /**
     * Get the current location.
     */
    public void getLocation() {
        FusedLocationProviderClient provider =
                LocationServices.getFusedLocationProviderClient(context);
        if (!isLocationEnabled()) showSettingAlert();
        else if (!checkPermission()) requestPermission();
        else provider.getLastLocation().addOnSuccessListener(this::get);
    }

    /**
     * Check if location service is enabled by LocationManager
     * @return true if location service is enabled
     */
    private boolean isLocationEnabled() {
        LocationManager manager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Show alert to ask user to enable location service.
     * If use choose yes, will open the setting page via Intent.
     */
    public void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage("Please enable location service.");
        alertDialog.setPositiveButton("Enable", (dialog, which) -> {
            Intent intent =
                    new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
        });
        alertDialog.setNegativeButton("Cancel", (dialog, which) ->
                dialog.cancel());
        alertDialog.show();
    }

    /**
     * Check if permission for getting location is given.
     * @return true if permission for getting location is given.
     */
    private boolean checkPermission() {
        int result1 = ActivityCompat.checkSelfPermission
                (context, Manifest.permission.ACCESS_FINE_LOCATION);
        int result2 = ActivityCompat.checkSelfPermission
                (context, Manifest.permission.ACCESS_COARSE_LOCATION);
        return result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request permission to get location if needed.
     */
    private void requestPermission() {
        ActivityCompat.requestPermissions((Activity) context,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
    }

    /**
     * Get lat/lng for a location and toast a message.
     * This method is attached as a OnSuccessListener
     * to the task returned from getLastLocation
     * @param location the current location got from FusedLocationProviderClient.
     */
    private void get(Location location){
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            Toast.makeText(context, "Marcos is at \n" +
                            "Lat " + latitude + "\nLong: " + longitude,
                    Toast.LENGTH_LONG).show();

        } else
            Toast.makeText(context, "Unable to get location",
                    Toast.LENGTH_LONG).show();

    }
}
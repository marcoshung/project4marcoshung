package edu.sjsu.android.project4template;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor> {

    // Uri: get from LocationsProvider
    private final Uri URI = LocationsProvider.CONTENT_URI;
    // 2 LatLng for SJSU and CS department
    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    private GoogleMap mMap;
    // (Extra credit) SharedPreferences and KEYs needed
    SharedPreferences preferences;
    private final String TYPE = "type";
    private final String LAT = "lat";
    private final String LNG = "lng";
    private final String ZOOM = "zoom";
    private int mapType;
    private LatLng position;
    private float zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        // Attach listeners to the buttons
        findViewById(R.id.location).setOnClickListener(this::getLocation);
        findViewById(R.id.uninstall).setOnClickListener(this::uninstall);
        findViewById(R.id.city).setOnClickListener(this::switchView);
        findViewById(R.id.univ).setOnClickListener(this::switchView);
        findViewById(R.id.cs).setOnClickListener(this::switchView);
        // Retrieve and draw already saved locations in map
        LoaderManager.getInstance(this).restartLoader(1, null, this);
        // Extra credit - restore the map setting using SharedPreferences
        preferences = getPreferences(MODE_PRIVATE);
        mapType = preferences.getInt(TYPE, GoogleMap.MAP_TYPE_NORMAL);
        double lat = preferences.getFloat(LAT, -200f);
        double lng = preferences.getFloat(LNG, -200f);
        if(lat != -200f && lng != -200f)
            position = new LatLng(lat, lng);
        zoom = preferences.getFloat(ZOOM, 0f);
    }

    // Extra credit - save map setting to shared preferences in onPause
    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(TYPE, mMap.getMapType());
        editor.putFloat(LAT, (float) mMap.getCameraPosition().target.latitude);
        editor.putFloat(LNG, (float) mMap.getCameraPosition().target.longitude);
        editor.putFloat(ZOOM, mMap.getCameraPosition().zoom);
        editor.apply();
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        // Extra credit - restore the map setting
        if(position != null){
            mMap.setMapType(mapType);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, zoom));
        }
        // on click, insert; on long click, delete
        mMap.setOnMapClickListener(this::insert);
        mMap.setOnMapLongClickListener(this::delete);
    }

    private void insert(LatLng point) {
        // 1) Add a maker on the point to the map
        mMap.addMarker(new MarkerOptions().position(point));
        // 2) Store the latitude, longitude and zoom level of the point to SQLite database using ContentValues
        ContentValues values = new ContentValues();
        values.put(LocationsDB.LAT, point.latitude);
        values.put(LocationsDB.LNG, point.longitude);
        values.put(LocationsDB.ZOOM, mMap.getCameraPosition().zoom);
        // 3) Call execute(ContentValues) on a MyTask (defined below) object to add a point
        new MyTask().execute(values);
    }

    private void delete(LatLng point) {
        // 1) Clear all markers from the Google Map
        mMap.clear();
        // 2) Call execute() on a MyTask (defined below) object to clear the database
        new MyTask().execute();
        // 3) Toast a message "All makers are removed"
        Toast.makeText(this, "All makers are removed", Toast.LENGTH_SHORT).show();
    }

    // Below is the class that extend AsyncTask, to insert/delete data in background
    // Note that AsyncTask is deprecated from API 30, but you can still use it.
    // You can use java.util.concurrent instead, if you are familiar with threads and concurrency.
    private class MyTask extends AsyncTask<ContentValues, Void, Void> {
        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            // Insert one row or delete all data base on input
            if (contentValues.length != 0 && contentValues[0] != null)
                getContentResolver().insert(URI, contentValues[0]);
            else getContentResolver().delete(URI, null, null);
            return null;
        }
    }
    // ----- End of AsyncTask classes -----


    // Below are for the CursorLoader, that is, the methods of
    // LoaderManager.LoaderCallbacks<Cursor> interface

    /**
     * Instantiate and return a new Loader for the database.
     *
     * @param id   the ID whose loader is to be created
     * @param args any arguments supplied by the caller
     * @return a new Loader instance that is ready to start loading
     */
    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        // Load the cursor that's pointing to the database
        return new CursorLoader(this, URI, null, null, null, null);
    }

    /**
     * Draw the markers after data is loaded, that is, the loader returned
     * in the onCreateLoader has finished its load.
     *
     * @param loader the Loader that has finished
     * @param cursor a cursor to read the data generated by the Loader
     */
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        // Display markers based on the database using cursor
        double lat, lng;
        float zoom;
        if (cursor.moveToFirst()) {
            // First, get data row by row using the cursor
            do {
                // For each row, get the latitude, longitude and zoom level
                lat = cursor.getDouble(1);
                lng = cursor.getDouble(2);
                zoom = cursor.getFloat(3);
                // Draw a marker based on the LatLng object on the map
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)));
            } while (cursor.moveToNext());
            // (Optional) Extra credit - if already defined from shared preferences
            // no need to move the camera
            if(position != null) return;
            // After getting all data, move the "camera" to
            // focus on the last clicked location (including zoom)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom));
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        // No need to implement
    }
    // ----- End of LoaderManager.LoaderCallbacks<Cursor> methods -----

    // Below are the methods respond to clicks on buttons
    // Remember to attach them to the buttons in onCreate
    // getLocation and switchView are the same as the ones in exercise 6.
    public void uninstall(View view) {
        Intent delete = new Intent(Intent.ACTION_DELETE,
                Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }

    public void getLocation(View view) {
        GPSTracker tracker = new GPSTracker(this);
        tracker.getLocation();
    }

    public void switchView(View view) {
        CameraUpdate update = null;
        if (view.getId() == R.id.city) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 10f);
        } else if (view.getId() == R.id.univ) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14f);
        } else if (view.getId() == R.id.cs) {
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18f);
        }
        assert update != null;
        mMap.animateCamera(update);
    }
}